# Thema Matrizenrechnungen

## Team Ieva Puplauskaite; Thi Ngoc Mai Pham; Sandhya Pawa

## Funktionsbeschreibung
- Kann Matrizen addieren, multiplizieren
- Eigenwerte ermitteln
- Inverse-Matrix bestimmen 
- Transponierte Matrix berechnen 


## Zeitplan
- Grobkonzept Ende Januar
- Feinkonzept Ende März

## Aufgabenteilung
- Alle Aufgaben werden gemeinsam bearbeitet.

## Zusammenarbeit
- Regelmäßige Treffen 14-tägig
- Informationsaustausch

## Dokumentation
- Basierend auf Javadoc
- Sprache Deutsch


## Offene Punkte, Fragen
- Entwicklungsumgebung eclipse statt BlueJ verwenden?

